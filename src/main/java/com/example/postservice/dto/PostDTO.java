package com.example.postservice.dto;


public class PostDTO {

    private Long user_id;
    private String note;
    private String title;

    public PostDTO(Long user_id, String note, String title) {
        this.user_id = user_id;
        this.note = note;
        this.title = title;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Long getUser_id() {
        return user_id;
    }

    public String getNote() {
        return note;
    }

    public String getTitle() {
        return title;
    }

}

