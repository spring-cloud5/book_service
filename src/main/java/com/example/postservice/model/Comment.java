package com.example.postservice.model;


import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "comments")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "note")
    private String note;

//    @Column(name = "post_id")
//    private Long post_id;

    @CreationTimestamp
    private Date date;

    @Column(name = "user_id")
    private Long user_id;

    @ManyToOne
    @JoinColumn(name="post_id", nullable=false)
    private Post post;

    public Comment(String note, Long user_id, Post post) {
        this.note = note;
        this.user_id = user_id;
        this.post = post;
    }
    public Comment(){}

    public void setId(Long id) {
        this.id = id;
    }

    public void setNote(String note) {
        this.note = note;
    }


    public void setDate(Date date) {
        this.date = date;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getId() {
        return id;
    }

    public String getNote() {
        return note;
    }


    public Date getDate() {
        return date;
    }

    public Long getUser_id() {
        return user_id;
    }
}