package com.example.postservice.payload;

import com.example.postservice.model.UserDTO;

import java.util.Date;
import java.util.List;

public class PostResponse  {

    private String  note;
    private String title;
    private String  url;
    private Date date;
    private UserDTO userDTO;
    private List<CommentResponse> comments;

    public PostResponse(String note, String title, String url, Date date, UserDTO userDTO, List<CommentResponse> comments) {
        this.note = note;
        this.title = title;
        this.url = url;
        this.date = date;
        this.userDTO = userDTO;
        this.comments = comments;
    }

    public PostResponse(String note, String title, String url, Date date, UserDTO userDTO) {
        this.note = note;
        this.title = title;
        this.url = url;
        this.date = date;
        this.userDTO = userDTO;
    }

    public void setComments(List<CommentResponse> comments) {
        this.comments = comments;
    }

    public List<CommentResponse> getComments() {
        return comments;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public String getNote() {
        return note;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public Date getDate() {
        return date;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }
}
