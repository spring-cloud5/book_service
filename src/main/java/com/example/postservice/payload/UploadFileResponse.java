package com.example.postservice.payload;


public class UploadFileResponse {
    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;
    private String user_name;
    private String user_id;
    private String titel;
    private String note;
    private String url;


    public String getUrl() {
        return url;
    }

    public UploadFileResponse(String fileName, String fileDownloadUri, String fileType, long size, String user_name, String user_id, String titel, String note , String url) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
        this.user_name = user_name;
        this.user_id = user_id;
        this.titel = titel;
        this.note = note;
        this.url = url;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getTitel() {
        return titel;
    }

    public String getNote() {
        return note;
    }

    public UploadFileResponse(String fileName, String fileDownloadUri, String fileType, long size, String user_name, String user_id) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
        this.user_name = user_name;
        this.user_id = user_id;
    }

    public UploadFileResponse(String fileName, String fileDownloadUri, String fileType, long size) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileDownloadUri() {
        return fileDownloadUri;
    }

    public void setFileDownloadUri(String fileDownloadUri) {
        this.fileDownloadUri = fileDownloadUri;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
