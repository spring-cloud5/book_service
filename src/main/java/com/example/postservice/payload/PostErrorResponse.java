package com.example.postservice.payload;


import java.util.List;

public class PostErrorResponse {

    private List<String> errors ;

    public PostErrorResponse(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
