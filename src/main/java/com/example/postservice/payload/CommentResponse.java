package com.example.postservice.payload;

import com.example.postservice.model.UserDTO;

public class CommentResponse {
    private String  note;
    private String date;
    private UserDTO userDTO;

    public CommentResponse(String note, String date, UserDTO userDTO) {
        this.note = note;
        this.date = date;
        this.userDTO = userDTO;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public String getNote() {
        return note;
    }

    public String getDate() {
        return date;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }
}
