package com.example.postservice.controller;

import com.example.postservice.dto.CommentDto;
import com.example.postservice.model.Comment;
import com.example.postservice.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("/createComment")
    Comment createComment(@RequestBody CommentDto comment){
        return this.commentService.createComment(comment);
    }
}
