package com.example.postservice.controller;

import com.example.postservice.dto.PostDTO;
import com.example.postservice.model.Post;
import com.example.postservice.payload.PostResponse;
import com.example.postservice.payload.UploadFileResponse;
import com.example.postservice.service.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")
public class PostController {


    @Autowired
    private PostService postService;

    // done and sent to rabbit mq postDTO and got validated
    @PostMapping("/createPost")
    public ResponseEntity<String> createPost(@RequestBody PostDTO postDTO) {
        return  postService.createPost(postDTO);
    }

    // after got valid we will store it
    @PostMapping("/storePost")
    public Post storePost(@RequestBody PostDTO postDTO) {
         return  postService.storePost(postDTO);
    }

    // index all posts
    @GetMapping("/getAll")
    public List<Post> getAllPosts () {
        return postService.getAllWithOutUsers();
    }

    // index all posts
    @GetMapping("/getAllWithUser")
    public List<Post> getAllPostsWithUsers () {
        return postService.getAllWithUsers();
    }

    // index user based posts
    @GetMapping("/getAllByUser/{user_id}")
    public List<Post> getAllByUser(@PathVariable Long user_id){
        return this.postService.getAllByUser(user_id);
    }

    // called by search service
    @PostMapping("/searchByKeyword")
    @CrossOrigin(origins = "*")
    public List<Post> getAllList(@RequestParam() String keyword){
        return postService.searchByKeyord(keyword);
    }
    // called by search service
    @PostMapping("/searchByKeywordWithoutUsers")
    @CrossOrigin(origins = "*")
    public List<Post> getAllListWithoutUsers(@RequestParam() String keyword){
        return postService.searchByKeyordWithoutUsers(keyword);
    }

//    @GetMapping("/getPost/{id}")
//    public PostResponse getPost (@PathVariable String id) {
//        return postService.getPostById(id);
//    }

//    @GetMapping("/getPostByUser/{user_id}")
//    public List<PostResponse> getPostUser (@PathVariable String user_id){
//        return postService.getPostUser(user_id);
//    }

}