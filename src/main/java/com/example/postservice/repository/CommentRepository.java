package com.example.postservice.repository;

import com.example.postservice.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CommentRepository extends JpaRepository<Comment, String> {

//    @Query("SELECT co FROM Comment co WHERE co.post_id=?1")
//    List<Comment> findCommentByPost_id(Long post_id);

}
