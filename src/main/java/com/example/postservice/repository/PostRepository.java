package com.example.postservice.repository;

import com.example.postservice.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, String> {

    Post findById(Long Id);

    @Query("SELECT f FROM Post f WHERE f.title LIKE %?1%"
            + " OR f.note LIKE %?1%")
    public List<Post> search(String keyword);

    @Query("SELECT p FROM Post p WHERE p.user_id=?1")
    List<Post> findByUser_id(Long user_id);
}