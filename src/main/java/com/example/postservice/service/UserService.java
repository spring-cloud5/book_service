package com.example.postservice.service;

import com.example.postservice.model.User;
import feign.hystrix.HystrixFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "userservice", fallback = UserService.UserServiceFallback.class)
public interface UserService {

    @GetMapping(value = "/getById/{id}")
    User getUserById(@PathVariable Long id);

    @Component
    static class UserServiceFallback implements UserService {

        @GetMapping(value = "/getById/{id}")
        public User getUserById(@PathVariable Long id) {
            return null;
        }
    }

}
