package com.example.postservice.service;

import com.example.postservice.model.Comment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collections;
import java.util.List;

@FeignClient(value = "commentservice", fallback = CommentServiceFeign.CommentServiceFeignFallBack.class)
public interface CommentServiceFeign {

    @GetMapping("/getComments/{post_id}")
    List<Comment> getComments(@PathVariable Long post_id);

    @Component
    static class CommentServiceFeignFallBack implements CommentServiceFeign{

        @Override
        public List<Comment> getComments(Long post_id) {
            return Collections.emptyList();
        }
    }
}
