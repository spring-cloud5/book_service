package com.example.postservice.service;

import com.example.postservice.dto.PostDTO;
import com.example.postservice.model.Comment;
import com.example.postservice.model.Post;
import com.example.postservice.model.UserDTO;
import com.example.postservice.payload.CommentResponse;
import com.example.postservice.payload.PostErrorResponse;
import com.example.postservice.payload.PostResponse;
import com.example.postservice.repository.PostRepository;
import com.netflix.discovery.converters.Auto;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.ServiceInstance;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Service
public class PostService {


    @Value("${rabbitmq.posts.exchange}")
    private String exchange;

    @Value("${rabbitmq.posts.routingkey}")
    private String routingKey;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentServiceFeign commentService;

    @Autowired
    private UserService userService;
//
    @Autowired
    AsyncService asyncService;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RabbitTemplate rabbitTemplate;


    // add store post request to the RabbitMq
    public ResponseEntity<String> createPost(PostDTO postInput) throws RestClientException {
//        RestTemplate restTemplate = new RestTemplate();
//        String baseUrl = getBaseUrlDiscoveryClient();
//
        rabbitTemplate.convertAndSend(exchange,routingKey,postInput);
        return new ResponseEntity<>("Post is Validating", HttpStatus.OK);
    }

    // print the validation mq request
    public Post storePost(PostDTO postDTO){
        return postRepository.save(new Post(postDTO.getTitle(),postDTO.getNote(),postDTO.getUser_id()));
    }

    // get all posts withoutUsers
    public List<Post> getAllWithOutUsers (){
        List<Post> posts = postRepository.findAll();
        for(Post post:posts){
            post.setComments(commentService.getComments(post.getId()));
        }
        return posts;
    }

    //get all with Users
    public List<Post> getAllWithUsers () {
        List<Post> posts = postRepository.findAll();
//        for(Post post : posts){
//            post.setComments(commentService.getComments(post.getId()));
//            post.setUser(userService.getUserById(post.getUser_id()));
//        }
        CompletableFuture<String> cf1 = asyncService.assertUsersToProjects(posts);
        CompletableFuture<String> cf2 = asyncService.assertCommentsToProjects(posts);

        CompletableFuture.allOf(cf1,cf2).join();

        return posts;
    }

    // my posts
    public List<Post> getAllByUser(Long user_id) {
        return this.postRepository.findByUser_id(user_id);
    }

    // search posts by keyword
        public List<Post> searchByKeyord(String keyword){
            List<Post> posts = postRepository.search(keyword);
            for(Post post : posts){
                post.setUser(userService.getUserById(post.getUser_id()));
            }
            return posts;
    }

    // search posts by keyword without
    public List<Post> searchByKeyordWithoutUsers(String keyword){
        List<Post> posts = postRepository.search(keyword);
        return posts;
    }

    private static HttpEntity<?> getHeaders() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        return new HttpEntity<>(headers);
    }

    private String getBaseUrlDiscoveryClient(){
        List<ServiceInstance> instances = discoveryClient.getInstances("service-route");
        ServiceInstance serviceInstance = instances.get(0);
        String baseUrl = serviceInstance.getUri().toString();
        return baseUrl;
    }

    public UserDTO getUser (String user_id) throws RestClientException{
        String baseUrl = getBaseUrlDiscoveryClient();
        baseUrl = baseUrl + "/user/api/getById/?id=" + user_id;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<UserDTO > response = null;
        try {
            response= restTemplate.exchange(baseUrl, HttpMethod.GET, getHeaders(), UserDTO.class);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return response.getBody();
    }

//    public PostResponse getPostById(String post_id){
//        Post post =  postRepository.getOne(post_id);
//        UserDTO userDTO = getUser(post.getUser_id().toString());
//        List<CommentResponse> commentResponses = new ArrayList<>();
//        commentResponses = commentService.getAll(post.getId());
//        return new PostResponse(post.getNote() , post.getTitle()," ",post.getDate() , userDTO , commentResponses);
//    }

//    public List<PostResponse> getPostUser(String user_id){
//        List<Post> posts = postRepository.findAllByUser_id(user_id);
//        return getAllWithUser(posts);
//    }

}