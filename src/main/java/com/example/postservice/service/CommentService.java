package com.example.postservice.service;

import com.example.postservice.dto.CommentDto;
import com.example.postservice.model.Comment;
import com.example.postservice.model.Post;
import com.example.postservice.payload.CommentResponse;
import com.example.postservice.repository.CommentRepository;
import com.example.postservice.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CommentService {


    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PostRepository postRepository;

    public Comment createComment(CommentDto comment) {
        Post post = postRepository.findById(comment.getPost_id());
        Comment newComment = new Comment(comment.getNote(),comment.getUser_id(),new Post(comment.getPost_id()));
        return commentRepository.save(newComment);
    }


//    public CommentResponse storeComment(CommentDto commentInput) throws RestClientException {
//        CommentResponse commentResponse = new CommentResponse(commentInput.getNote(),commentInput.getDate(),postService.getUser(commentInput.getUser_id()));
//        Comment comment = new Comment(commentInput.getNote() , commentInput.getPost_id(),commentInput.getDate(),commentInput.getUser_id());
//        commentRepository.save(comment);
//        return commentResponse;
//    }

//    private List<CommentResponse> getAllWithUser (List<Comment> comments){
//        List<CommentResponse> commentResponses= new ArrayList<>();
//        for(Comment comment : new ArrayList<>(comments)){
//            CommentResponse commentResponse = new CommentResponse(comment.getNote() , comment.getDate(), postService.getUser(comment.getUser_id()));
//            commentResponses.add(commentResponse);
//        }
//        return commentResponses;
//    }

//    public List<CommentResponse> getAll (Long post_id){
//        List<Comment> comments =  commentRepository.findCommentByPost_id(post_id);
//        return getAllWithUser(comments);
//    }


}
