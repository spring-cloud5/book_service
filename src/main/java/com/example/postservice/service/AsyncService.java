package com.example.postservice.service;

import com.example.postservice.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class AsyncService {

    @Autowired
    private CommentServiceFeign commentService;

    @Autowired
    private UserService userService;

    @Async
    public CompletableFuture<String> assertCommentsToProjects(List<Post> posts) {
        for (Post post : posts) {
            post.setComments(commentService.getComments(post.getId()));
        }
        return CompletableFuture.completedFuture("Done");
    }

    @Async
    public CompletableFuture<String> assertUsersToProjects(List<Post> posts) {
        for (Post post : posts) {
            post.setUser(userService.getUserById(post.getUser_id()));
        }
        return CompletableFuture.completedFuture("Done");
    }
}
